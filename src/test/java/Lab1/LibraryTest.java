/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Lab1;

import java.util.Arrays;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class LibraryTest {
    Directory root;

    @BeforeEach
    void setup() {
        root = new Directory();
        root.put("text", new TextFile("Hello, World!"));
        root.put("logs", new Directory());
        root.putByPath("logs/a.log", new LogFile());
        root.putByPath("logs/b.log", new LogFile());
        root.put("queue", new Fifo<String>());
    }

    @Test
    void get() {
        assertEquals("Hello, World!", root.get("text").<TextFile>cast().read());
    }

    @Test
    void logs() {
        LogFile log = (LogFile)root.getByPath("/logs/a.log");
        log.write("abc");
        log.write("def");

        assertLinesMatch(Arrays.asList("abc", "def"), log.read());
    }

    @Test
    void moveQueue() {
        Fifo<String> queue = root.get("queue").<Fifo<String>>cast();
        queue.write("hello");
        root.put("queue", new Directory());
        queue.moveTo("queue/1");

        assertEquals("hello", root.getByPath("/queue/1").<Fifo<String>>cast().try_read());
    }

    @Test
    void moveAboveRootError() {
        assertThrows(NullPointerException.class, () -> root.getByPath(".."));
    }

    @Test
    void relativePaths() {
        root.put("dir", new Directory());
        root.putByPath("dir/abc", new Directory());
        root.putByPath("dir/def", new Directory());

        assertEquals(root, root.getByPath("dir/abc").asDir().getByPath("../.."));
        assertEquals(root, root.getByPath("dir/abc").asDir().getByPath("./../.."));
        assertEquals(root, root.getByPath("dir/abc").asDir().getByPath("./../../"));
    }

    @Test
    void move() {
        FsNode text = root.get("text");
        text.moveTo("abcde");
        assertNull(root.get("text"));
        assertEquals(text, root.get("abcde"));
    }
}
