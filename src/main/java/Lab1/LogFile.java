package Lab1;

import java.util.List;
import java.util.ArrayList;


public class LogFile extends FsNode {
    List<String> log;

    public LogFile() {
        log = new ArrayList<>();
    }

    public void write(String line) {
        readwrite(() -> log.add(line));
    }

    public List<String> read() {
        return readonly(() -> new ArrayList<>(log));
    }
}
