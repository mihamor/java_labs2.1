package App;

import Lab1.*;

import java.util.concurrent.CyclicBarrier;


abstract class Task implements Runnable {
    Directory root;
    private CyclicBarrier barrier;

    public Task(Directory root, CyclicBarrier barrier) {
        this.root = root;
        this.barrier = barrier;
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            barrier.await();
            task_finalize();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {

        }
    }

    public void task_init() throws InterruptedException {}

    public void task() throws InterruptedException {}

    public void task_finalize() throws InterruptedException {}
}

class Thread1 extends Task {
    public Thread1(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() {
        Directory dir = new Directory();
        root.put("task1", dir);

        for (int i = 0; i < 5; i++) {
            dir.put(Integer.toString(i), new TextFile("file" + i));
            sleep(500);
        }
    }

    public void task() {
        for (int i = 0; i < 5; i++) {
            FsNode file = root.getByPath("/task1/" + i);
            file.moveTo(file.<TextFile>cast().read());
            sleep(100);
        }
    }
}

class Thread2 extends Task {
    public Thread2(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() {
        sleep(1000);
        root.put("task2queue", new Fifo<String>());
    }

    public void task() throws InterruptedException {
        while (true) {
            String msg = root.getByPath("task2queue").<Fifo<String>>cast().wait_read();
            if (msg.equals("exit")) {
                System.out.println("t2 exit");
                break;
            } else {
                System.out.println("t2 message: " + msg);
            }
        }
    }
}

class Thread3 extends Task {
    public Thread3(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task() {
        for (int i = 0; i < 5; i++) {
            root.getByPath("task2queue").<Fifo<String>>cast().write("message" + i);
            sleep(300);
        }
        root.getByPath("task2queue").<Fifo<String>>cast().write("exit");
    }

    public void task_finalize() {

    }
}

class Thread4 extends Task {
    public Thread4(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() {
        root.put("task4log", new LogFile());
    }

    public void task() {
        sleep(200);
        root.getByPath("task1").asDir().list()
                .forEach(entry -> root.get("task4log").<LogFile>cast().write("first " + entry.fst));

        sleep(400);
        root.getByPath("task1").asDir().list()
                .forEach(entry -> root.get("task4log").<LogFile>cast().write("second " + entry.fst));
    }

    public void task_finalize() {
        System.out.println("t4 log:");
        for (String line : root.get("task4log").<LogFile>cast().read()) {
            System.out.println("t4 " + line);
        }
    }
}

class Thread5 extends Task {
    public Thread5(Directory root, CyclicBarrier b) {
        super(root, b);
    }

    public void task_init() {

    }

    public void task() {

    }

    public void task_finalize() {

    }
}



public class App {
    public static void main(String[] args) throws Exception {
        CyclicBarrier barrier = new CyclicBarrier(6);
        Directory root = new Directory();

        new Thread(new Thread1(root, barrier), "1").start();
        new Thread(new Thread2(root, barrier), "2").start();
        new Thread(new Thread3(root, barrier), "3").start();
        new Thread(new Thread4(root, barrier), "4").start();
        new Thread(new Thread5(root, barrier), "5").start();

        barrier.await();
        barrier.await();

        System.out.println("\n----\nroot:\n" + root.show(1));
    }
}
